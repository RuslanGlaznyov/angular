import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AppComponent } from './app.component';
import { ModalDialogComponent } from './modalWindow/modal-page.component';
import { HttpClientModule } from '@angular/common/http';
import { TasksService } from './tasks/tasks.service';
import { TasksComponent} from './tasks/tasks.component'
import {RouterModule, Routes} from '@angular/router';
import {SignInComponent} from './login/sign-in/sign-in.component';
import {AuthService} from './auth/auth.service';
import {AuthGuard} from './auth/auth.guard';
import {LoginComponent} from "./login/login.component";
import {RegisterComponent} from "./login/register/register.component";
import {IdentityRevealedValidatorDirective} from "./login/register/MatchPassword.directive";

const loginRouters: Routes = [
  { path: 'sign-in', component: SignInComponent },
  { path: 'register', component: RegisterComponent },
  { path:'', redirectTo: 'sign-in' , pathMatch: 'full'}
];

const appRoutes: Routes = [
  { path: '', component: TasksComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent, children: loginRouters},
  { path: '**', redirectTo: 'login' }
  ];


@NgModule({
  declarations: [
    AppComponent,
    TasksComponent,
    ModalDialogComponent,
    SignInComponent,
    LoginComponent,
    RegisterComponent,
    SignInComponent,
    IdentityRevealedValidatorDirective,
  ],

  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    ReactiveFormsModule
  ],
  providers: [TasksService, AuthService, AuthGuard],
  bootstrap: [AppComponent]
})
export class AppModule { }
