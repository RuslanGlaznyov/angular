import {Component, OnInit} from '@angular/core';
import {TasksService} from './tasks.service';
import {TaskInterfaces} from "../interfases/task.interfaces";


@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.css']
})
export class TasksComponent implements OnInit {

  done: boolean = false;
  showDialog: boolean = false;


  tasks: TaskInterfaces[] = [];
  currentTask: TaskInterfaces;

  constructor(private taskService: TasksService) {
  }


  addTask = (task: TaskInterfaces) => {
    this.taskService.postTask(task.title, task.desc, this.done)
      .subscribe(
        () => {
          this.taskService.getTasks().subscribe((data: any) => this.tasks = data);
        },
        error => console.log(error));
  };

  editTask = (task: TaskInterfaces) => {
    this.currentTask = undefined;

    this.taskService.putTask(task.title, task.desc, task.done, task._id)
      .subscribe(
        () => {
          this.taskService.getTasks().subscribe((data: any) => this.tasks = data);
        },
        error => {
          task.done = !task.done;
          console.log(error);
        }
      );
  };

  deleteTask = (task: TaskInterfaces) => {
    this.taskService.delTask(task._id)
      .subscribe(
        () => {
          this.taskService.getTasks().subscribe((data: any) => this.tasks = data);
        },
        error => console.log(error)
      );
  };

  openWindow() {
    this.showDialog = true;
  }

  onClose() {
    this.currentTask = undefined;
    this.showDialog = false;
  }

  showCurrentTask(task: TaskInterfaces) {
    this.showDialog = !this.showDialog;
    this.currentTask = task;
  }

  ngOnInit() {
    this.taskService.getTasks().subscribe((data: any) => this.tasks = data);
  }

}
