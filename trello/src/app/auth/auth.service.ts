import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
  constructor(private http: HttpClient) { }

  // login(email: string, password: string) {
  //   const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded',
  //     'authorization': password})
  //   return this.http.get(`http://localhost:3000/user/login/${email}`, {headers});
  // }
  // ;
  login(email: string, password: string) {
    const headers = new HttpHeaders({'Content-Type': 'application/json',
      'authorization': password});

    return this.http.get<any>(`http://localhost:3000/user/login/${email}`, { headers})
      .pipe(map(user => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          return user;
      }
      ));
  }

  register(user: any) {
    return this.http.post<any>('http://localhost:3000/user/', user)
      .pipe(map(user => {
          localStorage.setItem('currentUser', JSON.stringify(user));
          return user;
        }
      ));
  }

  logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('currentUser');
  }
}
