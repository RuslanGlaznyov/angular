import {Component, Injectable, OnDestroy, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {identityRevealedValidator} from "./MatchPassword.directive";
import {AuthService} from "../../auth/auth.service";
import {Router} from "@angular/router";
import {emailRegex} from "../regexEmail.const";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
})

@Injectable()
export class RegisterComponent implements  OnInit {
  form: FormGroup;
  isEmailExist: boolean = false;

  constructor (
    private authService: AuthService,
    private router: Router,
  ) { };

  newUser = {
    firstName: String,
    lastName: String,
    email: "",
    password: "",
  };

  ngOnInit() {
    this.form = new FormGroup({
    'newUserEmail': new FormControl('', [
        Validators.required,
        Validators.pattern(emailRegex),
      ]
    ),
    'firstName': new FormControl('', Validators.required),
    'lastName': new FormControl('', Validators.required),
    'newPassword': new FormControl('', [Validators.required, Validators.minLength(6)]),
    'checkPassword': new FormControl('', [Validators.required, Validators.minLength(6)])
  }, { validators: identityRevealedValidator })
  }


  get newPassword() { return this.form.get('newPassword'); }

  get checkPassword() { return this.form.get('checkPassword'); }

  register() {
    this.newUser = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      email: this.form.value.newUserEmail,
      password: this.form.value.checkPassword,

    }
    this.authService.register(this.newUser).subscribe(
      data => {
        this.authService.login(this.newUser.email, this.newUser.password).subscribe(
          data => {
            this.router.navigate(['']);
          },
          error => {
            this.isEmailExist = true;
          }
        );
      },
      error => {
        this.isEmailExist = true;
      });


  }
}


