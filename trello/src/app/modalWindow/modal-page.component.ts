import {Input, Output, Component, EventEmitter} from '@angular/core';
import {TaskInterfaces} from "../interfases/task.interfaces";

@Component({
    selector     : 'app-modal-dialog',
    templateUrl: './modal-page.component.html',
    styleUrls: ['./modal-page.component.css'],


})

export class ModalDialogComponent {


  @Input() addTask: Function;
  @Input() editTask: Function;
  modalTask: any;
  isNewTask: boolean;

  @Input('setCurrentTask') set test(value) {

    if (value) {
      this.isNewTask = false;
      this.modalTask = Object.assign({}, value);
    } else {
      this.isNewTask = true;
      this.modalTask = new TaskInterfaces();
    }
  }

  @Input()  closable = true;
  @Input()  visible: boolean;
  @Output() visibleChange: EventEmitter<boolean> = new EventEmitter<boolean>();


  close() {
    this.modalTask = {};
    this.visible = false;
    this.visibleChange.emit(this.visible);
  }

}
