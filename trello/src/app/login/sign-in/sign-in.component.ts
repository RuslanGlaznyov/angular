import {Component, Injectable, OnInit} from '@angular/core';
import {FormGroup, FormControl, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../auth/auth.service';
import {emailRegex} from "../regexEmail.const";

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
@Injectable()
export class SignInComponent implements OnInit {

  // user: User = new User();
  form: FormGroup;

  isIncorrectEmailPass: boolean;

  constructor(
      private route: ActivatedRoute,
      private router: Router,
      private authService: AuthService,
    ) {
    this.form = new FormGroup({
      'userEmail': new FormControl('', [
        Validators.required,
        Validators.pattern(emailRegex),
      ]),
      'userPassword': new FormControl('', [Validators.required, Validators.minLength(6)]),
    });
  }
  ngOnInit() {
    this.authService.logout();
  }


  login() {
    this.authService.login(this.form.value.userEmail, this.form.value.userPassword)
      .subscribe(
        data => {
          this.router.navigate(['']);
        },
        error => {
          this.isIncorrectEmailPass = true;
        });
  }


}


