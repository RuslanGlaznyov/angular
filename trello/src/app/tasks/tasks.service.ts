import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable()
export class TasksService{

    constructor(private http: HttpClient){ }

    getTasks() {
      let localUser = JSON.parse(localStorage.getItem('currentUser'));
      let token = 'bearer ' + localUser.token;
      const headers = new HttpHeaders({'Content-Type': 'application/json',
        'authorization': token});
      return this.http.get(`http://localhost:3000/task/${localUser.userId}`, {headers});
    }
    postTask(title, desc, done) {
      let localUser = JSON.parse(localStorage.getItem('currentUser'));
      let token = 'bearer ' + localUser.token;
      const headers = new HttpHeaders({'Content-Type': 'application/json',
        'authorization': token});

      const body = {title: title, desc: desc, done: done, userId: localUser.userId};
      return this.http.post('http://localhost:3000/task/', body, {headers});
    }
    putTask(title, desc, done, id) {
      let localUser = JSON.parse(localStorage.getItem('currentUser'));
      let token = 'bearer ' + localUser.token;
      const headers = new HttpHeaders({'Content-Type': 'application/json',
        'authorization': token});
      const body = {title: title, desc: desc, done: done};

      return this.http.put('http://localhost:3000/task/' + id, body, {headers});
    }

    delTask(id) {
      let localUser = JSON.parse(localStorage.getItem('currentUser'));
      let token = 'bearer ' + localUser.token;
      const headers = new HttpHeaders({'Content-Type': 'application/json',
        'authorization': token});

      return this.http.delete('http://localhost:3000/task/' + id, {headers});
    }

}
